/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.zongtui.web.modules.echartsdemo.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zongtui.web.common.web.BaseController;

/**
 * echarts demoController
 * @author zhangfeng
 * @version 2015-05-02
 */
@Controller
@RequestMapping(value = "${adminPath}/echartsdemo/echartsdemo")
public class EchartsdemoController extends BaseController {
	
	/**
	 * list:bar应用. <br/>
	 *
	 * @author Administrator
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @since JDK 1.7
	 */
	@RequestMapping(value = {"bar", ""})
	public String bar(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/barList";
	}
	
	@RequestMapping(value = {"bar1", ""})
	public String bar1(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar1List";
	}
	
	@RequestMapping(value = {"bar2", ""})
	public String bar2(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar2List";
	}
	
	@RequestMapping(value = {"bar3", ""})
	public String bar3(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar3List";
	}
	
	@RequestMapping(value = {"bar4", ""})
	public String bar4(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar4List";
	}
	
	@RequestMapping(value = {"bar5", ""})
	public String bar5(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar5List";
	}
	
	@RequestMapping(value = {"bar6", ""})
	public String bar6(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar6List";
	}
	
	@RequestMapping(value = {"bar7", ""})
	public String bar7(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar7List";
	}
	
	@RequestMapping(value = {"bar8", ""})
	public String bar8(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar8List";
	}
	
	@RequestMapping(value = {"bar9", ""})
	public String bar9(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar9List";
	}
	
	@RequestMapping(value = {"bar10", ""})
	public String bar10(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar10List";
	}
	
	@RequestMapping(value = {"bar11", ""})
	public String bar11(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar11List";
	}
	
	@RequestMapping(value = {"bar12", ""})
	public String bar12(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar12List";
	}
	
	@RequestMapping(value = {"bar13", ""})
	public String bar13(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar13List";
	}
	
	@RequestMapping(value = {"bar14", ""})
	public String bar14(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar14List";
	}
	
	@RequestMapping(value = {"bar15", ""})
	public String bar15(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/bar/bar15List";
	}
	
	
	
	/**
	 * 线图（广告来源）
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"line", ""})
	public String line(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/line/lineList";
	}
	
	/**
	 * 线图（气温）
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"line1", ""})
	public String line1(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/line/line1List";
	}
	
	@RequestMapping(value = {"line2", ""})
	public String line2(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/line/line2List";
	}
	
	@RequestMapping(value = {"line3", ""})
	public String line3(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/line/line3List";
	}
	
	@RequestMapping(value = {"line4", ""})
	public String line4(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/line/line4List";
	}
	
	@RequestMapping(value = {"line5", ""})
	public String line5(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/line/line5List";
	}
	
	@RequestMapping(value = {"line6", ""})
	public String line6(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/line/line6List";
	}
	
	@RequestMapping(value = {"line7", ""})
	public String line7(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/line/line7List";
	}
	
	@RequestMapping(value = {"line8", ""})
	public String line8(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/line/line8List";
	}
	
	@RequestMapping(value = {"line9", ""})
	public String line9(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/line/line9List";
	}
	
	@RequestMapping(value = {"line10", ""})
	public String line10(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/line/line10List";
	}
	
	@RequestMapping(value = {"line11", ""})
	public String line11(HttpServletRequest request, HttpServletResponse response, Model model) {
		return "modules/echartsdemo/line/line11List";
	}
	

}