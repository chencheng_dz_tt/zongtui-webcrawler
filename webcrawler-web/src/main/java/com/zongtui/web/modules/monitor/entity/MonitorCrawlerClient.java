/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.zongtui.web.modules.monitor.entity;

import org.hibernate.validator.constraints.Length;

import com.zongtui.web.common.persistence.DataEntity;

/**
 * 客户端监控Entity
 * @author zhangfeng
 * @version 2015-05-02
 */
public class MonitorCrawlerClient extends DataEntity<MonitorCrawlerClient> {
	
	private static final long serialVersionUID = 1L;
	private String crawlerId;		// crawler_id
	private String crawlerIp;		// crawler_ip
	private String crawlerName;		// crawler_name
	private Integer crawlerStatus;		// crawler_status
	
	public MonitorCrawlerClient() {
		super();
	}

	public MonitorCrawlerClient(String id){
		super(id);
	}

	@Length(min=1, max=50, message="crawler_id长度必须介于 1 和 50 之间")
	public String getCrawlerId() {
		return crawlerId;
	}

	public void setCrawlerId(String crawlerId) {
		this.crawlerId = crawlerId;
	}
	
	@Length(min=0, max=50, message="crawler_ip长度必须介于 0 和 50 之间")
	public String getCrawlerIp() {
		return crawlerIp;
	}

	public void setCrawlerIp(String crawlerIp) {
		this.crawlerIp = crawlerIp;
	}
	
	@Length(min=0, max=100, message="crawler_name长度必须介于 0 和 100 之间")
	public String getCrawlerName() {
		return crawlerName;
	}

	public void setCrawlerName(String crawlerName) {
		this.crawlerName = crawlerName;
	}
	
	public Integer getCrawlerStatus() {
		return crawlerStatus;
	}

	public void setCrawlerStatus(Integer crawlerStatus) {
		this.crawlerStatus = crawlerStatus;
	}
	
}