<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>echarts demo管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>

</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/echartsdemo/echartsdemo/bar3">echarts demo列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="echartsdemo" action="${ctx}/echartsdemo/echartsdemo/bar3" method="post" class="breadcrumb form-search">
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	
	<div id="main" style="height:400px"></div>
	<script src="${ctxStatic}/echarts/echarts.js"></script>
<script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '${ctxStatic}/echarts'
            }
        });
        
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/line', // 使用线状图就加载line模块，按需加载
                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main')); 
                
                var option = {
                	    title : {
                	        text: '温度计式图表',
                	        subtext: 'From ExcelHome',
                	        sublink: 'http://e.weibo.com/1341556070/AizJXrAEa'
                	    },
                	    tooltip : {
                	        trigger: 'axis',
                	        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                	            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                	        },
                	        formatter: function (params){
                	            return params[0].name + '<br/>'
                	                   + params[0].seriesName + ' : ' + params[0].value + '<br/>'
                	                   + params[1].seriesName + ' : ' + (params[1].value + params[0].value);
                	        }
                	    },
                	    legend: {
                	        selectedMode:false,
                	        data:['Acutal', 'Forecast']
                	    },
                	    toolbox: {
                	        show : true,
                	        feature : {
                	            mark : {show: true},
                	            dataView : {show: true, readOnly: false},
                	            restore : {show: true},
                	            saveAsImage : {show: true}
                	        }
                	    },
                	    calculable : true,
                	    xAxis : [
                	        {
                	            type : 'category',
                	            data : ['Cosco','CMA','APL','OOCL','Wanhai','Zim']
                	        }
                	    ],
                	    yAxis : [
                	        {
                	            type : 'value',
                	            boundaryGap: [0, 0.1]
                	        }
                	    ],
                	    series : [
                	        {
                	            name:'Acutal',
                	            type:'bar',
                	            stack: 'sum',
                	            barCategoryGap: '50%',
                	            itemStyle: {
                	                normal: {
                	                    color: 'tomato',
                	                    barBorderColor: 'tomato',
                	                    barBorderWidth: 6,
                	                    barBorderRadius:0,
                	                    label : {
                	                        show: true, position: 'insideTop'
                	                    }
                	                }
                	            },
                	            data:[260, 200, 220, 120, 100, 80]
                	        },
                	        {
                	            name:'Forecast',
                	            type:'bar',
                	            stack: 'sum',
                	            itemStyle: {
                	                normal: {
                	                    color: '#fff',
                	                    barBorderColor: 'tomato',
                	                    barBorderWidth: 6,
                	                    barBorderRadius:0,
                	                    label : {
                	                        show: true, 
                	                        position: 'top',
                	                        formatter: function (params) {
                	                            for (var i = 0, l = option.xAxis[0].data.length; i < l; i++) {
                	                                if (option.xAxis[0].data[i] == params.name) {
                	                                    return option.series[0].data[i] + params.value;
                	                                }
                	                            }
                	                        },
                	                        textStyle: {
                	                            color: 'tomato'
                	                        }
                	                    }
                	                }
                	            },
                	            data:[40, 80, 50, 80,80, 70]
                	        }
                	    ]
                	};
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
            }
        );
    </script>
	
</body>
</html>